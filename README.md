# Setting up the environment

Before moving on, install anaconda and clone this repository to your working directory.

We need packages from `conda-forge` so add it to your channels:

```
conda config --add channels conda-forge
```


Once `conda-forge` has been enabled, install the `opalenv.yml` environment like so:

```
conda env create -f opalenv.yml
```

Now you have distgen, OPAL, and openPMD-beamphysics installed. Add the following to your `.bash_profile` so that 
everything's initialized upon login, or enter it manually to start creating your own distributions, running OPAL 
simulations, and postprocessing your data. 

```
source activate openPMD-beamphysics
cd /path/to/working/directory
source "conda/envs/OPAL-2.4.0/etc/profile.d/opal.sh"
```

# Creating a particle distribution
To create your distribution:

```
python gendist.py
```

You can change details in the `./lcls-lattice-master/distgen/models/sc_inj/v0/distgen.yaml` file to make a different distribution, and change the last line in `gendist.py` to export the distribution file to a different name. This 
is the input file you'll be referencing in the .in file.


# Running an OPAL simulation
For this example, we'll be simulating the LCLS-II photoinjector so navigate to `/lcls-lattice-master/opal/models/sc_inj_C8`

```
cd /lcls-lattice-master/opal/models/sc_inj_C8
```

The distribution you made should be in this directory. Make sure its name matches up with the input for FNAME in line 152. Then run the OPAL simulation like so:

```
opal sc_inj_C8.in 
```

If you have several .in files in your current working directory that you want to simulate, you can do so recursively with this command:

```
for file in ./*.in; do opal "$file"; done
```

This will output .h5 file(s) to be sent to the postprocessor.

# Postprocessing the data
The postprocessor I developed fits the experimental data to 1st, 2nd, and 3rd order polynomials (depending on the argument given), and outputs their plots as well as the fitted coefficient values, and several goodness-of-fit 
test statistics. Here's an example of a command used to produce a 3rd order fit:

```
python fitting.py sc_inj_C8.h5 3rd
```

If you made simulations recursively, chances are you'll also want to produce these fits recursively, so here is the command for that:

```
for file in ./*.h5; do python polyfit.py "$file" 3rd; done
```
