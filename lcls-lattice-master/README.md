# lcls-lattice

Lattice and input files for the LCLS accelerators (including the LCLS-II project)

Many of the paths will use the `$LCLS_LATTICE` environmental variable, which should point to this repository, as in:

`export LCLS_LATTICE=/path/to/this/repo`


