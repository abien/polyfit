# Distgen cu_inj beam



## v0
Reference input, variant 0

This roughly reconstructs the reference Impact-T parcl.data file in:

`$LCLS/lattice/impact/models/cu_inj`

px, py distributions show excellent agreement, with MTE = 414 meV

pz in `partcl.data` is completely unmodeled. 

x, y are radial Gaussian with a sigma of 0.4 mm and a cutoff at 1.5 sigma.


## `2gauss`: Two stacked Gaussians







