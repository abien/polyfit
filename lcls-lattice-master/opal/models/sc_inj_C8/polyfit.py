# To keep information in plot legend consistent, denote phase
# angle in the first 3 characters of the .h5 file name
# Ex. +20_sc_inj_C1.h5


import matplotlib
import opal
import os
import sys
import numpy as np
import scipy.stats as stats
from scipy.optimize import curve_fit
from h5py import File
import pmd_beamphysics
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from pmd_beamphysics.plot import marginal_plot, slice_plot
from pmd_beamphysics.interfaces import opal
from pmd_beamphysics import ParticleGroup, particle_paths
matplotlib.rcParams['figure.figsize'] = (8,6)
H5FILE = './' + sys.argv[1]
h5 = File(H5FILE, 'r')
ph5_group = h5[list(h5.keys())[-1]]
ph5 = opal.opal_to_data(ph5_group)
P = ParticleGroup(data = ph5)

#Experimental data definitions
xData = P.z/np.linalg.norm(P.z)
yData = P.pz/np.linalg.norm(P.pz)

#Conditional fitting functions with curve fitting
if sys.argv[2] == '1st':
  def func(x, a, b):
    return a+b*x
  popt, pcov = curve_fit(func, xData, yData)
  patch = mpatches.Patch(color = 'white', label = 'Fitting Parameters: \n a=%3f \n b=%3f' %tuple(popt))
elif sys.argv[2] == '2nd':
  def func(x, a, b, c):
    return a+b*x+c*x**2
  popt, pcov = curve_fit(func, xData, yData)
  patch = mpatches.Patch(color = 'white', label = 'Fitting Parameters: \n a=%3f \n b=%3f \n c=%3f' %tuple(popt))
elif sys.argv[2] == '3rd':
  def func(x, a, b, c, d):
    return a+b*x+c*x**2+d*x**3
  popt, pcov = curve_fit(func, xData, yData)
  patch = mpatches.Patch(color = 'white', label = 'Fitting Parameters: \n a=%3f \n b=%3f \n c=%3f \n d=%3f' %tuple(popt))

#y values for the fit function (plugging in xData since OPAL positions are produced dynamically)
yFit = func(xData, *popt)

#Plot data
plt.plot(xData, yData, 'bo', label='OPAL output at ' + H5FILE[4:7] + chr(176) + ' phase')

#Plot the fit
plt.plot(xData, yFit, 'ro', label='Fit data')
plt.xticks(fontsize = 8)
plt.xlabel('z [mm]')
plt.ylabel('pz [MeV/c]')

#Legend formatting
handles, labels = plt.gca().get_legend_handles_labels()
#patch = mpatches.Patch(color='white', label='Fitting Parameters: \n a=%3f \n b=%3f \n c=%3f \n d=%3f' % tuple(popt))
handles.append(patch) 
plt.legend(handles = handles, loc = 'best')


#Save plot
plt.savefig('./' + sys.argv[2] + '_order_fits/' + H5FILE[:-3] + '.pdf')
original_stdout = sys.stdout # Save a reference to the original standard output

with open('./' + sys.argv[2] + '_order_fits/' + H5FILE[:-3] +  '_' + sys.argv[2] + '_order_fit_stats.txt', 'w') as f:
    sys.stdout = f
    print(stats.linregress(yData, yFit))
    print(stats.chisquare(yData, yFit))
    print(stats.cramervonmises_2samp(yData, yFit))
    print(stats.ks_2samp(yData, yFit))
    print(stats.epps_singleton_2samp(yData, yFit))
    print(stats.anderson_ksamp([yData, yFit]))
    if sys.argv[2] == '1st':
      print('a=%3f\nb=%3f' % tuple(popt))
    elif sys.argv[2] == '2nd':
      print('a=%3f\nb=%3f\nc=%3f' % tuple(popt))
    elif sys.argv[2] == '3rd':
      print('a=%3f\nb=%3f\nc=%3f\nd=%3f' % tuple(popt))
    sys.stdout = original_stdout # Reset the standard output to its original value
