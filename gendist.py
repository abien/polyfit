import distgen
from distgen import Generator
dist_file = './lcls-lattice-master/distgen/models/sc_inj/v0/distgen.yaml'
dist = Generator(input=dist_file, verbose=False)
dist.input['n_particle'] = int(5e4)
dist.run()
particles = dist.particles 
print(dist)
# you can save the particles to file if you want
particles.write_opal('./lcls-lattice-master/opal/models/sc_inj_C8/opal_50k.particles') 
